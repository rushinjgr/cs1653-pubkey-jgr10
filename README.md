I used gpg to generate an RSA keypair with a 4096 bit key that expires in one year.


I used that key length because that is the length that Bruce Schneier recently switched to.

See: http://pgp.mit.edu:11371/pks/lookup?search=schneier&op=index

This is may be overkill for a key that is expiring in one year, however.

It is certainly good enough if respected experts are using a similar key length for a long term key.

